# This is gammu Module which is needed to communicate with the dongle
import gammu
 
# Set up the connection to the dongle using gammu
sm = gammu.StateMachine()

# Reading of gammu configration
sm.ReadConfig()
sm.Init()

# meaasge body of SMS
message_body = """Allah Akbar
Mohamed Rasol Allah
    Sent by Rasp-Alert"""

# Recipent Number
mynumber = '+201022466667'
 
# Set the message we want to send
message = {'Text': message_body, 'SMSC': {'Location': 1}, 'Number': mynumber,}
 
# Send of the message
sm.SendSMS(message)
