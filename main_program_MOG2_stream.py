#!/home/pi/manager-project
# python main_program.py

# import of necessary Modules
from multiprocessing import Manager, Process
import time
from processesfolder import socket_server, motion_detection_MOG2_Stream

if __name__ == "__main__":
    # Define manager object called (manage)  that contols a server process
    # which holds python objects and allow other processes to manipulate them using proxies
    manage = Manager()

    # Declaration of Manager dictionary 
    manage_dict = manage.dict()
    manage_dict['Mod_flag'] = False
    print "Initialization Manager"
    ###########################
    manage_dict['Profile_flag'] = 'Profile_1'
    print "before process:", manage_dict
    # single manager can be shared by processes on different computers over a network
    # that make it slower than using shared memory.

    # Define the processes that will work in parallelism behaviour
    # motion_detection_program = Process(target = motion_detection_upvid_modify.start_detection, args=(manage_dict,))
    modifier_socket = Process(target=socket_server.server_listening, args=(manage_dict,))
    motion_detection_MOG2_program = Process(target=motion_detection_MOG2_Stream.start_detection, args=(manage_dict,))
    # stream_program = Process(target = app.start_flask, args=(manage_dict,))

    # set the process's daemon boolean flag to be True before start() is called
    modifier_socket.daemon = True
    time.sleep(5)
    # motion_detection_program.daemon = True
    motion_detection_MOG2_program.daemon = True
    # time.sleep(5)
    # stream_program.daemon = True

    # Starting of processes
    # motion_detection_program.start()
    modifier_socket.start()
    if manage_dict['Profile_flag'] != "Profile_3":
        motion_detection_MOG2_program.start()
    print "after processes started:", manage_dict

    # Wait for worker processes to exit
    # motion_detection_program.join()
    modifier_socket.join()
    if manage_dict['Profile_flag'] != "Profile_3":
        motion_detection_MOG2_program.join()
    print "after processes join:", manage_dict
