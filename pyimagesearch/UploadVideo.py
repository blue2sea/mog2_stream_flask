# import the necessary packages
from collections import deque
from threading import Thread
# from dropbox.client import DropboxOAuth2FlowNoRedirect
# from dropbox.client import DropboxClient
import time
import cv2


class UploadVideo:
    def __init__(self):
        # initialize the buffer of frames, queue of frames that
        # need to be written to file, video writer, writer thread,
        # and boolean indicating whether recording has started or not
        self.thread = None
        self.uploading = False

    def start(self, client, path, outputPath):
        # indicate that we are recording, start the video writer,
        # and initialize the queue of frames that need to be written
        # to the video file
        self.uploading = True
        self.thread = Thread(target=self.upload(client, path, outputPath), args=())
        self.thread.daemon = True
        self.thread.start()

    # upload()
    # client.put_file(path, open(outputPath, "rb"))
    # start a thread write frames to the video file

    def upload(self, client, path, outputPath):
        # keep looping
        while True:
            # if we are done recording, exit the thread
            if not self.uploading:
                return

            client.put_file(path, open(outputPath, "rb"))
            self.finish()

    def finish(self):
        # indicate that we are done uploading, join the thread,
        # flush all remaining frames in the queue to file, and
        # release the writer pointer
        self.uploading = False

        # self.thread.join()
