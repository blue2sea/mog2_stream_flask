#!/usr/bin/env python
from flask import Flask, render_template, g, Response,flash,redirect, request, url_for, session, abort

# emulated camera
#from camera import Camera

# Raspberry Pi camera module (requires picamera package)
from camera_pi import Camera

def start_flask(manage_dict):
    app = Flask(__name__)
    if manage_dict['Profile_flag'] == "Profile_3":
        print "sTREAM ok..."
        @app.route('/', methods=['GET', 'POST'])
        def login():
            error = None
            
            if request.method == 'POST':
                if request.form['username'] != "islamsaad":
                    error = 'Invalid username'
                elif request.form['password'] != "Smart@2017":
                    error = 'Invalid password'
                else:
                    session['logged_in'] = True
                    flash('You were logged in')
                    return render_template("about.html")
            return render_template('login.html', error=error)


        @app.route('/logout')
        def logout():
                session.pop('logged_in'. None)
                flash('You were logged out')
                return redirect(url_for('show_entries'))

        #@app.route('/')
        #def index():
        #    """Video streaming home page."""
        #    return render_template('smart.html')

        # bOUND OF FUNCTION
        @app.route('/about')
        def about():
            """Video streaming home page."""
            return render_template('about.html')

        @app.route('/stream')
        def stream():
        #   """Video streaming home page."""
            return render_template('stream.html')

        @app.route('/config')
        def config():
            """Video streaming home page."""
            return render_template('config.html')

        @app.route('/help')
        def help():
            """Video streaming home page."""
            return render_template('help.html')


        #Genertator Function for sending frame by frame Which captured by PICamera
        def gen(camera):
            """Video streaming generator function."""
            while True:
                frame = camera.get_frame()
                yield (b'--frame\r\n'
                       b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

        @app.route('/video_feed')
        def video_feed():
            """Video streaming route. Put this in the src attribute of an img tag."""
            return Response(gen(Camera()),
                            mimetype='multipart/x-mixed-replace; boundary=frame')

    else:
        print "Stream OFF.."
        # Server Socket Connection
    if __name__ != '__main__':
        app.secret_key = 'super secret key'
        app.config['SESSION_TYPE'] = 'filesystem'
        app.run(host='0.0.0.0', port=5000, debug=True, threaded=True)
