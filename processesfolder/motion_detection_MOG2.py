# USAGE
# python motion_detection_upvid.py

# import the necessary packages
from pyimagesearch.UploadVideo import UploadVideo
from pyimagesearch.keyclipwriter import KeyClipWriter
#from imutils.video import VideoStream
#import argparse
#from dropbox.client import DropboxOAuth2FlowNoRedirect
#from dropbox.client import DropboxClient
import dropbox
from picamera.array import PiRGBArray
from picamera import PiCamera
import warnings
import datetime
import imutils
import json
import thread
import time
import cv2
#from sms_sinch import send_smssinch
from send_email import send_email
#from sms_dongle import send_smsdongle

def start_detection(manage_dict):

        while True:
                
                # filter warnings, load the configuration and initialize the Dropbox
                # client
                warnings.filterwarnings("ignore")
                current_profile = manage_dict['Profile_flag']
                print "Openning of json file ....." , manage_dict['Profile_flag']
                if manage_dict['Profile_flag'] == "Profile_1":
                        conf = json.load(open("Profile_1.json", "rb"))
                      
                if manage_dict['Profile_flag'] == "Profile_2":
                        conf = json.load(open("Profile_2.json", "rb"))
                        
                if manage_dict['Profile_flag'] == "Profile_3":
                        conf = json.load(open("Profile_3.json", "rb"))
                print "Profile Loaded ...", manage_dict
                
                use_dropbox = conf["use_dropbox"]
                dropbox_key = conf["dropbox_key"]
                dropbox_secret = conf["dropbox_secret"]
                dropbox_accessToken = conf["accessToken"]
                dropbox_path = conf["dropbox_base_path"]
                sleep_time = conf["camera_warmup_time"]
                resolution = tuple(conf["resolution"])
                framerate = conf["fps"]
                buffer_size = conf["buffer_size"]
                delta_threshold = conf["delta_thresh"]
                min_area = conf["min_area"]
                min_motion_frames = conf["min_motion_frames"]
                min_upload_seconds = conf["min_upload_seconds"]
                output_video_path = conf["output"]
                codec_unicode = conf["codec"]
                codec_ascii = codec_unicode.encode('ascii')
                codec_ch = list(codec_ascii)
                speed = conf["speed"]
                show_video = conf["show_video"]
               
                email_enable = True
                sms_sinch_enable = True
                sms_dongle_enable = True
                motionsequence_recorded = False 
                client = None

                # check to see if the Dropbox should be used
                if use_dropbox:
                        # connect to dropbox and start the session authorization process
                        #flow = DropboxOAuth2FlowNoRedirect(dropbox_key, dropbox_secret)

                        # finish the authorization and grab the Dropbox client 29-10-17
                        #client = DropboxClient(dropbox_accessToken)
                        client = dropbox.Dropbox(dropbox_accessToken)
                        print "[SUCCESS] dropbox account linked" #, client.users_get_current_account()
                        
                # initialize the camera and grab a reference to the raw camera capture
                camera = PiCamera()
                camera.resolution = resolution
                camera.framerate = framerate
                rawCapture = PiRGBArray(camera, size = resolution)
                camera.rotation  = 270
                camera.hflip = True
                

                # allow the camera to warmup, then initialize the average frame, last
                # uploaded timestamp, and frame motion counter
                #print "[INFO] warming up..."
                print("[INFO] warming up camera...")
                time.sleep(sleep_time)
                avg = None
                lastcheck = datetime.datetime.now()
                lastUploaded = datetime.datetime.now()
                motionCounter = 0

                # initialize key clip writer and the consecutive number of
                # frames that have *not* contained any action
                kcw = KeyClipWriter(bufSize = buffer_size)
                consecFrames = 0

                # keep looping
                #while True:
                        # grab the current frame, resize it, and initialize a
                        # boolean used to indicate if the consecutive frames
                        # counter should be updated
                #	frame = vs.read()
                ### CREAT INSTANCE OF SB
                fgbg = cv2.createBackgroundSubtractorMOG2(history=200, varThreshold=16, detectShadows=True)
                fgbg.setNMixtures(3)

                for f in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True, ):
                        # grab the raw NumPy array representing the image and initialize
                        # the timestamp and occupied/unoccupied text
                        frame = f.array
                        timestamp = datetime.datetime.now()
                  #exposure# Swapping EXPOSURE_MODES depend on daylight
                        if timestamp.strftime("%H%M")<1900 and timestamp.strftime("%H%M")>0600:
                                camera.exposure_mode = 'auto'
                                print " AUTO mode"
                        else:
                                camera.exposure_mode = 'night'

                        
########################################################################################################
## checking every frame for modify flag and new profile isn't current profile
                        if manage_dict['Mod_flag'] and manage_dict['Profile_flag']!=current_profile:
                                print "Loop Breaking"
                                manage_dict['Mod_flag'] = False                                    
                                camera.close()
                                break
########################################################################################################
                        text = "Unoccupied"
                        


                        # Apply MOG Algorithm on Frames sequence Loop
                        fgmask = fgbg.apply(frame)
                        """
                        # resize the frame, convert it to grayscale, and blur it
                        frame = imutils.resize(frame, width=500)
                        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                        gray = cv2.GaussianBlur(gray, (21, 21), 0) # 11,11 instead of 21,21

                        # if the average frame is None, initialize it
                        if avg is None:
                                print "[INFO] starting background model..."
                                avg = gray.copy().astype("float")
                                rawCapture.truncate(0)
                                continue

                        # accumulate the weighted average between the current frame and
                        # previous frames, then compute the difference between the current
                        # frame and running average
                        cv2.accumulateWeighted(gray, avg, 0.5)
                        frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(avg))

                        # threshold the delta image, dilate the thresholded image to fill
                        # in holes, then find contours on thresholded image
                        thresh = cv2.threshold(frameDelta, delta_threshold , 255,
                                cv2.THRESH_BINARY)[1]
                        thresh = cv2.dilate(thresh, None, iterations=2)
                        """
                        (_, cnts, _) = cv2.findContours(fgmask.copy(), cv2.RETR_EXTERNAL,
                                cv2.CHAIN_APPROX_SIMPLE)

                        # loop over the contours
                        for c in cnts:
                                # if the contour is too small, ignore it
                                if cv2.contourArea(c) < min_area:
                                        continue

                                # compute the bounding box for the contour, draw it on the frame,
                                # and update the text
                                (x, y, w, h) = cv2.boundingRect(c)
                                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                                text = "Occupied"
                        # draw the text and timestamp on the frame
                        ts = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
                        cv2.putText(frame, "Room Status: {}".format(text), (10, 20),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 255), 2)
                        cv2.putText(frame, ts, (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX,
                                0.45, (0, 255, 255), 1)

                        if text == "Occupied":
                        # increment the motion counter
                                motionCounter += 1
                                consecFrames = 0
#################################################################################################################################
                                if (motionCounter >= min_motion_frames and email_enable == True):
                                        email_enable = False
                                        print "Motion Detected MOG Mail!"
                                        send_email("SMART_SURVEILLANCE system", "Intrusion is detected!")
#####################################################################################################################################
                                if (motionCounter >= min_motion_frames and sms_sinch_enable == True):
                                        print "Motion Detected sinch!"
                                        sms_sinch_enable = False
                                        #send_smssinch()
#####################################################################################################################################
                                if (motionCounter >= min_motion_frames and sms_dongle_enable == True):
                                        sms_dongle_enable = False
                                        #send_smsdongle()
#################################################################################################################################                                        
                                #updateConsecFrames = False
                                # if we are not already recording, start recording
                                if not kcw.recording and motionCounter >= min_motion_frames:
                                        timestamp = datetime.datetime.now()
                                        p = "{}/{}.avi".format(output_video_path,
                                                timestamp.strftime("%Y%m%d-%H%M%S"))
                                        kcw.start(p, cv2.VideoWriter_fourcc(codec_ch[0], codec_ch[1], codec_ch[2], codec_ch[3]), speed)
                                        motionsequence_recorded = True

                        # otherwise, no action has taken place in this frame, so
                        # increment the number of consecutive frames that contain
                        # no action
                        if text == "Unoccupied":
                        # Reset the motion counter
                                motionCounter = 0
                                consecFrames += 1

                        # update the key frame clip buffer
                        kcw.update(frame)

                        # if we are recording and reached a threshold on consecutive
                        # number of frames with no action, stop recording the clip
                        if kcw.recording and consecFrames == buffer_size:
                                kcw.finish()
                                if email_enable == False:
                                        send_email("SMART_SURVEILLANCE system", "No INTRUSION NOW:)")
                                
###########################################################################################################################3333
                                        email_enable = True
                                        sms_sinch_enable = False
                                        sms_dongle_enable = True
                                
################################################################################################################################
                                # UPLOAD USING dropbox
                                if use_dropbox and motionsequence_recorded:
                                        # upload the image to Dropbox and cleanup the tempory image
                                        if (timestamp - lastUploaded).seconds >= min_upload_seconds:

                                                #print client
                                                print "[UPLOADING] {}".format(ts)
                                                path = "{base_path}/{timestamp}.avi".format(
                                                                base_path= dropbox_path, timestamp=ts)
                                                ##########################################
                                                def upload(upth, client, path, outputPath):
                                                        #client.put_file(path, open(outputPath, "rb"))
                                                        client.files_upload(open(outputPath, "rb").read(), path)
                                                        print "[UPLOADED] {}".format(ts)
                                                thread.start_new_thread(upload, ("upth", client, path, p,))	
                                                ##########################################
                                                lastUploaded = datetime.datetime.now()
                                
                        # check to see if the frames should be displayed to screen
                        if show_video:
                                # display the security feed
                                cv2.imshow("Security Feed", frame)
                                key = cv2.waitKey(1) & 0xFF

                                # if the `q` key is pressed, break from the lop
                                if key == ord("q"):
                                        break

                        # clear the stream in preparation for the next frame
                        rawCapture.truncate(0)
	
                # if we are in the middle of recording a clip, wrap it up
                if kcw.recording:
                        kcw.finish()

                # do a bit of cleanup
                cv2.destroyAllWindows()

#############%%%%%%%%%%%%%%%%%%%%%%
#start_detection()
#############%%%%%%%%%%%%%%%%%%%%%%

